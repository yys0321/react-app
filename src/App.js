import Cars from './components/Cars'
import Hello from './components/Hello'

function App() {
  return (
    <div>
      <Hello />
      <Cars />
    </div>
  )
}

export default App