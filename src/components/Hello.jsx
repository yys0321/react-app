import React, { useState } from 'react'

import './Hello.css'

const Hello = () => {
    const [name, setName] = useState('')
    return (
        <div className="container">
            <div>Enter your name: {name}</div>
            <input className="input" type="text" value={name} onChange={(event) => setName(event.target.value)} />
            <button className="button">Submit</button>
        </div>
    )
}

export default Hello