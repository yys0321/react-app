useEffect is a React hook that manages side effects in functional components.
It handles tasks like fetching data, subscribing to events, or manipulating the DOM.
It is a replacement for lifecycle methods in class components and enables users to define actions after rendering and clean up resources when the component is unmounted.
